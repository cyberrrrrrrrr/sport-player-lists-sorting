#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#define rsize 10
#define csize 3

//copy the file's contents more easily
struct player {
	char num[50];
	char name[50];
	char team[50];
	char height[50];
	char weight[50];
	char position[50];
	char age[50];
};

//count the amount of players in the file
int amountofplayers() {
	FILE *file = fopen("listofplayers.txt", "r");
	if (!file)
		return 0;
	char buf[1000];
	int count = 0;
	while (fgets(buf, 1000, file) != NULL) {
		count++;
	}
	return (count+1)/8;
}

//fill the struct with data from the file
void fillstruct(struct player *players) {
	FILE *file = fopen("listofplayers.txt", "r");
	if (!file)
		return;
	char buf[512];
	int i = 0;
	while (fgets(buf, sizeof(buf), file) != NULL) {
		buf[strlen(buf) - 1] = '\0';
		if (!strncmp(buf, "name", 4)) {
			strncpy(players[i].name, buf + 5, 50);
		}
		if (!strncmp(buf, "team", 4)) {
			strncpy(players[i].team, buf + 5, 50);
		}
		if (!strncmp(buf, "height", 6)) 
			//players[i].height = atof(buf + 7);
			strncpy(players[i].height, buf + 7, 50);
		if (!strncmp(buf, "weight", 6)) 
			//players[i].weight = atoi(buf + 7);
			strncpy(players[i].weight, buf + 7, 50);
		if (!strncmp(buf, "position", 8)) {
			strncpy(players[i].position, buf + 9, 50);
		}
		if (!strncmp(buf, "age", 3)) {
			//players[i].age = atoi(buf + 4);
			strncpy(players[i].age, buf + 4, 50);
			sprintf(players[i].num, "Player %d", i + 1);
			i++;
		}
	}
	fclose(file);
}

//regularly print everything
void printplayers(struct player *players) {
	for (int i = 0; i < rsize; i++) {
		printf("%s\n%s\n%s\n%s\n%s\n%s\n%s\n\n\n", players[i].num, players[i].name, players[i].team, players[i].height, players[i].weight, players[i].position, players[i].age);
		}
}

//print the array
printarr(char *arr[3][10]) {
	for (int i = 0; i < rsize; i++) {
		printf("\n");
		for (int j = 0; j < csize; j++) {
			printf("%s | ", arr[j][i]);
		}
	}
	printf("\n");
}

//show list of players
void listofplayers(struct player *players) {
	char *arr[3][10] = {
	{players[0].num, players[1].num, players[2].num, players[3].num, players[4].num, players[5].num, players[6].num, players[7].num, players[8].num, players[9].num},
	{players[0].name, players[1].name, players[2].name, players[3].name, players[4].name, players[5].name, players[6].name, players[7].name, players[8].name, players[9].name },
	{players[0].position, players[1].position, players[2].position, players[3].position, players[4].position, players[5].position, players[6].position, players[7].position, players[8].position, players[9].position }
	};
	printf("\nORIGINAL LIST");
	printarr(arr);
	printf("END OF ORIGINAL LIST\n");
}

///QUICKSORT BLOCK
//swap rows in the array
void swap(char *arr[3][10], int a, int b) {
	char *r = arr[0][a];
	arr[0][a] = arr[0][b];
	arr[0][b] = r;
	char *s = arr[1][a];
	arr[1][a] = arr[1][b];
	arr[1][b] = s;
	char *t = arr[2][a];
	arr[2][a] = arr[2][b];
	arr[2][b] = t;
}

int partition(char *arr[3][10], int low, int high) {
	double pivot = atof(arr[2][high]);
	int i = (low - 1);
	for (int j = low; j <= high - 1; j++) {
		if (atof(arr[2][j]) <= pivot) {
			i++;
			swap(arr, i, j);
		}
	}
	swap(arr, i + 1, high);
	return (i + 1);
}

void quicksort(char *arr[3][10], int low, int high) {
	if (low < high) {
		int i = partition(arr, low, high);
		quicksort(arr, low, i - 1);
		quicksort(arr, i + 1, high);
	}
}
///END OF QUICKSORT BLOCK

//show sorted list by height (using quick sort)
void sortbyheight(struct player *players) {
	char *arr[3][10] = {
		{ players[0].num, players[1].num, players[2].num, players[3].num, players[4].num, players[5].num, players[6].num, players[7].num, players[8].num, players[9].num },
		{ players[0].name, players[1].name, players[2].name, players[3].name, players[4].name, players[5].name, players[6].name, players[7].name, players[8].name, players[9].name },
		{ players[0].height , players[1].height, players[2].height, players[3].height, players[4].height, players[5].height, players[6].height, players[7].height, players[8].height, players[9].height }
	};
	printf("\nORIGINAL LIST");
	printarr(arr);
	printf("END OF ORIGINAL LIST\n");
	quicksort(arr, 0, 9);
	printf("\nSORTED LIST");
	printarr(arr);
	printf("END OF SORTED LIST\n");
}

///MERGESORT BLOCK
void merge(char *arr[3][10], int l, int m, int r){
	int i, j, k;
	char *L[3][5];
	char *R[3][5];
	for (i = 0; i < 5; i++) {
		L[0][i] = arr[0][l + i];
		L[1][i] = arr[1][l + i];
		L[2][i] = arr[2][l + i];
	}
	for (j = 0; j < 5; j++) {
		R[0][j] = arr[0][m + 1 + j];
		R[1][j] = arr[1][m + 1 + j];
		R[2][j] = arr[2][m + 1 + j];
	}
	i = 0; 
	j = 0; 
	k = l; 
	while (i < 5 && j < 5){
		if (atoi(L[2][i]) <= atoi(R[2][j])){
			arr[0][k] = L[0][i];
			arr[1][k] = L[1][i];
			arr[2][k] = L[2][i];
			i++;
		}
		else{
			arr[0][k] = R[0][j];
			arr[1][k] = R[1][j];
			arr[2][k] = R[2][j];
			j++;
		}
		k++;
	}
	while (i < 5){
		arr[0][k] = L[0][i];
		arr[1][k] = L[1][i];
		arr[2][k] = L[2][i];
		i++;
		k++;
	}
	while (j < 5){
		arr[0][k] = R[0][j];
		arr[1][k] = R[1][j];
		arr[2][k] = R[2][j];
		j++;
		k++;
	}
}

void mergesort(char *arr[3][10], int l, int r){
	if (l < r){
		int m = l + (r - l) / 2;
		mergesort(arr, l, m);
		mergesort(arr, m + 1, r);
		merge(arr, l, m, r);
	}
}
///END OF MERGESORT BLOCK

//show sorted list by weight (using merge sort)
void sortbyweight(struct player *players) {
	char *arr[3][10] = {
		{ players[0].num, players[1].num, players[2].num, players[3].num, players[4].num, players[5].num, players[6].num, players[7].num, players[8].num, players[9].num },
		{ players[0].name, players[1].name, players[2].name, players[3].name, players[4].name, players[5].name, players[6].name, players[7].name, players[8].name, players[9].name },
		{ players[0].weight , players[1].weight, players[2].weight, players[3].weight, players[4].weight, players[5].weight, players[6].weight, players[7].weight, players[8].weight, players[9].weight }
	};
	printf("\nORIGINAL LIST");
	printarr(arr);
	printf("END OF ORIGINAL LIST\n");
	quicksort(arr, 0, 9);
	printf("\nSORTED LIST");
	printarr(arr);
	printf("END OF SORTED LIST\n");
}

///INSERTIONSORT BLOCK
void insertionsort(char *arr[3][10], int n){
	int i, j;
	char *key[3];
	for (i = 1; i < n; i++){
		key[0] = arr[0][i];
		key[1] = arr[1][i];
		key[2] = arr[2][i];
		j = i - 1;
		while (j >= 0 && atoi(arr[2][j]) > atoi(key[2])){
			arr[0][j + 1] = arr[0][j];
			arr[1][j + 1] = arr[1][j];
			arr[2][j + 1] = arr[2][j];
			j = j - 1;
		}
		arr[0][j + 1] = key[0];
		arr[1][j + 1] = key[1];
		arr[2][j + 1] = key[2];
	}
}
///END OF INSERTIONSORT BLOCK

//show sorted list by age (using insertion sort)
void sortbyage(struct player *players) {
	char *arr[3][10] = {
		{ players[0].num, players[1].num, players[2].num, players[3].num, players[4].num, players[5].num, players[6].num, players[7].num, players[8].num, players[9].num },
		{ players[0].name, players[1].name, players[2].name, players[3].name, players[4].name, players[5].name, players[6].name, players[7].name, players[8].name, players[9].name },
		{ players[0].age , players[1].age, players[2].age, players[3].age, players[4].age, players[5].age, players[6].age, players[7].age, players[8].age, players[9].age }
	};
	printf("\nORIGINAL LIST");
	printarr(arr);
	printf("END OF ORIGINAL LIST\n");
	insertionsort(arr, 10);
	printf("\nSORTED LIST");
	printarr(arr);
	printf("END OF SORTED LIST\n");
}


int main() {
	struct player *players;
	players = (struct player *)malloc(amountofplayers() * sizeof(struct player));
	int input = 0;
	while (1) {
		printf("\n1.Show list of players\n2.Show sorted list by height\n3.Show sorted list by weight\n4.Show sorted list by age\n5.Exit\n");
		fillstruct(players);
		scanf("%d", &input);
		switch (input) {
		case 1:
			listofplayers(players);
			break;
		case 2:
			sortbyheight(players);
			break;
		case 3:
			sortbyweight(players);
			break;
		case 4:
			sortbyage(players);
			break;
		case 5:
			exit(1);
		}
	}
	printf("\n\n");
	system("PAUSE");
}